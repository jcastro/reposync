# Reposync job

To add new repos, add the repo file to [prod.repos.d](prod.repos.d/), the
GPG key to [gpgkeys](gpgkeys/) and any specific configuration to [prod.repos.yaml](prod.repos.yaml).
This last part is probably not needed.

If you're adding a redhat repo, you probably also need the SSL client certificate.
 1. Download the certificates, if necessary (see below)
 1. Add the certificate to Teigi: `tbag set --hg lxsoft/adm 8a85f983598e8558015993b62b96699e.pem --file 8a85f983598e8558015993b62b96699e.pem`
 1. List the new certificate in `manifests/adm.pp` for the lxsoft machines.
 1. Make sure your new repo files in [prod.repos.yaml](prod.repos.yaml) list the new certificate. You can use something like this to figure out which certificates belong to with repos:

(execute on an ADM node with the certificates)
```
for i in `ls /etc/cdn.redhat.com/*.pem`; do printf "$i returned http_code: "; curl -k -E $i https://cdn.redhat.com/content/dist/rhel/server/7/7.5/x86_64/os/repodata/ --write-out %{http_code} --silent --output /dev/null; printf "\n"; done
```


# Downloading Redhat certificates

Certs for linuxsoft-mirror system registered on [RHN](https://access.redhat.com/management/systems/b4ec8c2d-3eae-4ae0-b8fa-ec6d8a08ce9f/subscriptions)

```
8a85f9845993af3f015993b34c3f0210 - 2017-01-01 - 2020-01-01 Red Hat Enterprise Linux Server, Self-support (1-2 sockets) (Up to 1 guest)
8a85f983598e8558015993b62b96699e - 2017-01-01 - 2020-01-01 Extended Update Support
8a85f9875993915c015993b8460b1956 - 2017-01-01 - 2020-01-01 Red Hat Enterprise Linux Developer Suite
8a85f983598e8558015993be99386c0f - 2017-01-01 - 2020-01-01 Red Hat JBoss A-MQ, 64-Core Standard
8a85f9825cc471b3015cc47ecc80054c - 2017-06-20 - 2020-01-01 Red Hat Virtualization (2-sockets), Premium
8a85f983598e8558015993c40f836ef2 - 2017-01-01 - 2020-01-01 Red Hat Enterprise MRG Realtime, Standard (1-2 sockets)
8a85f9875b339bfe015b33aaa17019fc - 2017-04-03 - 2020-01-01 Red Hat Enterprise Linux Extended Life Cycle Support (Physical or Virtual Nodes)
```

Note: with each new/changed subscription we have to add/remove subscription for linuxsoft-mirror
on RHN and use freshly regenerated cert .. seems to be necessary also in case of new product
versions which appeared after the orig. cert was generated

removed/replaced certs:

```
8a85f98159926149015993c2a4ed781a - 2017-01-01 - 2020-06-20 Red Hat Virtualization (2-sockets), Premium
8a85f983598e8558015993be99386c0f - replaced 2018-02-27 for RH-SSO 7.2
d0ef2de33635419fbf7467a54ba485c9 - replaced 2019-08-16 for Extended Update Support
```

You can use the following command to determine what certificate maps to which entitlement:
```
# for i in *pem; do echo -n "$i: "; subscription-manager import --certificate $i >/dev/null; subscription-manager list --consumed |grep "Subscription Name" | cut -d: -f2; subscription-manager remove --all >/dev/null; done
8a85f9825cc471b3015cc47ecc80054c.pem:    Red Hat Virtualization (2-sockets), Premium
8a85f983598e8558015993be99386c0f.pem:    Red Hat AMQ, Standard (64 Cores)
8a85f983598e8558015993c40f836ef2.pem:    Red Hat Enterprise MRG Realtime, Standard (1-2 sockets)
8a85f9845993af3f015993b34c3f0210.pem:    Red Hat Enterprise Linux Server, Self-support (1-2 sockets) (Up to 1 guest)
8a85f9875993915c015993b8460b1956.pem:    Red Hat Enterprise Linux Developer Suite
8a85f9875b339bfe015b33aaa17019fc.pem:    Red Hat Enterprise Linux Extended Life Cycle Support (Physical or Virtual Nodes)
97a00645e90241a495c87c71cab7258f.pem:    Red Hat Virtualization Manager
d0ef2de33635419fbf7467a54ba485c9.pem:    Extended Update Support
#
```

## Procedure (Update 2018/04):

1. Download the zip with all certificates
1. Rename them to the subject (be careful, the following may need to be adapted as Subject format may change)
```bash
for i in `ls *.pem`; do  NAME=`openssl x509 -in $i -text | grep -i "Subject:" | sed 's/.*CN *= *\([a-z0-9]\{32\}\).*/\1/'`; mv $i $NAME.pem; done
```
3. Proceed with step 2 above, adding the certificates to Teigi.
