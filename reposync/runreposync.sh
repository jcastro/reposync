#!/bin/bash

OUTPUT='/alloc/output.json'

log () {
  mapfile IN
  cat << EOF | tr -d '\n'; echo
{ "repoid": "${REPOID}",
  ${IN[@]}
}
EOF
}

error () {
  mapfile IN
  cat << EOF | log
    "message_type": "error",
    ${IN[@]}
EOF
}

echo "Inputs: REPOID=\"$REPOID\" REPOFILE=\"$REPOFILE\" REPOPATH=\"$REPOPATH\" CHECKSUM=\"$CHECKSUM\" RUN_REPOSYNC=\"$RUN_REPOSYNC\" RUN_CREATEREPO=\"$RUN_CREATEREPO\""

echo "[${REPOID}]" > /etc/yum.repos.d/sync.repo
echo $REPOFILE | base64 -d >> /etc/yum.repos.d/sync.repo

# Don't try to download asynchronously
# There's a crappy bug in that async code somewhere, this avoids it
printf "\nasync=0\n" >> /etc/yum.repos.d/sync.repo

REPOPATH="/repo/${REPOPATH}"

# Create REPOPATH even if the repo is empty
[ ! -d $REPOPATH ] && /bin/mkdir -p ${REPOPATH}

if [[ $RUN_REPOSYNC -eq 1 ]]; then
  # Record what's already there first
  PRECOUNT=`/usr/bin/find ${REPOPATH} -type f -name "*.rpm" ! -size 0 | /usr/bin/wc -l`
  PRELIST=`/usr/bin/find ${REPOPATH} -type f -name "*.rpm" ! -size 0 -exec /bin/basename {} \;`
  PRESIZE=`/usr/bin/du -bc Packages/*rpm *rpm source/SRPMS/*rpm source/SRPMS/Packages/*rpm 2>/dev/null | /usr/bin/tail -1 | /bin/sed "s/\stotal//g"`

  grep 'baseurl=' /etc/yum.repos.d/sync.repo | grep -q 'aarch64'
  if [ $? -eq 0 ]; then
    AARCH64="--arch=aarch64"
  fi

  # Run reposync
  /usr/bin/reposync \
    --downloadcomps \
    --download-metadata \
    --norepopath \
    --source \
    --gpgcheck \
    --download_path ${REPOPATH} \
    ${AARCH64} \
    --repoid ${REPOID} 2> /local/stderr
  RET=$?

  if [[ $RET -eq 2 ]]; then
    cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "reposync gpg error",
    "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
  elif [[ $RET -ne 0 ]]; then
    cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "reposync failed",
    "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
    exit $RET
  fi

  # Remove 0 byte RPMs
  /usr/bin/find ${REPOPATH} -type f -size 0b -name "*.rpm" -exec rm -v {} \;

  # Now let's look at the new stuff
  POSTCOUNT=`/usr/bin/find ${REPOPATH} -type f -name "*.rpm" ! -size 0 | /usr/bin/wc -l`
  POSTLIST=`/usr/bin/find ${REPOPATH} -type f -name "*.rpm" ! -size 0 -exec /bin/basename {} \;`
  # cdn.redhat.com delivers us some rpms with junk attached at the end ... ?
  # these are 'useable' but since the size changes, metadata must be regenerated to
  # take it into account.
  POSTSIZE=`/usr/bin/du -bc Packages/*rpm *rpm source/SRPMS/*rpm source/SRPMS/Packages/*rpm 2>/dev/null | /usr/bin/tail -1 | /bin/sed "s/\stotal//g"`

  # Sync GPG key from repo config file. Put it on repo root path
  grep -q 'gpgkey=' /etc/yum.repos.d/sync.repo
  if [ $? -eq 0 ]; then
    GPGKEYPATH=$(grep -oP "gpgkey=file://\K(.*)" /etc/yum.repos.d/sync.repo)
    REPOROOTPATH=$(echo $REPOPATH | awk -F "/" '{print "/"$2"/"$3"/"$4"/"}')
    cp -u $GPGKEYPATH $REPOROOTPATH
  fi
fi

if [[ ($PRECOUNT -eq $POSTCOUNT) && ($PRESIZE -ne $POSTSIZE) ]]; then
  cat << EOF | error | tee $OUTPUT
    "error": "WARNING: no rpm count change but size change (pre: $PRESIZE, post: $POSTSIZE)"
EOF
fi

if [[ ($PRECOUNT -ne $POSTCOUNT) || ($PRESIZE -ne $POSTSIZE) ]]; then
  CHANGELIST=`/usr/bin/diff --changed-group-format='%>' --unchanged-group-format='' <(echo "$PRELIST") <(echo "$POSTLIST")`

  # Run createrepo
  if [[ $RUN_CREATEREPO -eq 1 ]]; then
    COMPXMLFILE=`/bin/ls -1tr ${REPOPATH}/*-comps*xml.gz  2>&1 | /usr/bin/tail -1`
    MODULEFILE=`/bin/ls -1tr ${REPOPATH}/*-modules*yaml.gz  2>&1 | /usr/bin/tail -1`

    if [ -f "$COMPXMLFILE" ]; then
      /bin/cp -f $COMPXMLFILE $REPOPATH/comps.xml.gz
      /bin/gunzip -f $REPOPATH/comps.xml.gz
      /bin/rm -f $REPOPATH/*-comps*xml.gz
      GROUPFILE="-g ${REPOPATH}/comps.xml"
    fi

    # Run createrepo
    /usr/bin/createrepo \
      --workers 5 \
      --checksum ${CHECKSUM} \
      ${GROUPFILE} \
      --outputdir ${REPOPATH} \
      ${REPOPATH} 2> /local/stderr

    RET=$?
    if [[ $RET -ne 0 ]]; then
      cat << EOF | error | tee $OUTPUT
        "exit_code": $RET,
        "error": "createrepo failed",
        "output": "$(sed 's/"/\\"/g' /local/stderr)"
EOF
      exit $RET
    fi

    /bin/rm -f $REPOPATH/comps.xml

    UPDXMLFILE=`/bin/ls -1tr $REPOPATH/*-updateinfo.xml.gz 2>&1 | /usr/bin/tail -1`
    if [ -f "$UPDXMLFILE" ]; then
      /bin/cp -f $UPDXMLFILE $REPOPATH/updateinfo.xml.gz
      /bin/gunzip -f $REPOPATH/updateinfo.xml.gz
      /bin/rm -f $REPOPATH/*-updateinfo.xml.gz

      /usr/bin/modifyrepo $REPOPATH/updateinfo.xml $REPOPATH/repodata
    fi
    if [ -f "$MODULEFILE" ]; then
      /bin/cp -f $MODULEFILE $REPOPATH/modulefile.yaml.gz
      /bin/gunzip -f $REPOPATH/modulefile.yaml.gz
      /bin/rm -f $REPOPATH/modulefile.yaml.gz

      /usr/bin/modifyrepo --mdtype=modules $REPOPATH/modulefile.yaml $REPOPATH/repodata
    fi
  fi # end of if [[ $RUN_CREATEREPO -eq 1 ]]

else # else of if [[ $PRECOUNT -ne $POSTCOUNT ]]
  # No new packages
  /bin/rm -f $REPOPATH/*-updateinfo.xml.gz $REPOPATH/*-comps*xml.gz $REPOPATH/*-updateinfo.xml
fi



for rpm in ${CHANGELIST}; do
  path=`/usr/bin/find ${REPOPATH} -type f -name "${rpm}"`
  info=`/usr/bin/rpm --queryformat "%{NAME} %{VERSION}-%{RELEASE} %{ARCH}" -qp ${path} 2>/dev/null`
  name=`echo $info | cut -d' ' -f1`
  version=`echo $info | cut -d' ' -f2`
  arch=`echo $info | cut -d' ' -f3`

  cat << EOF | log
    "message_type": "change",
    "rpm_name": "${name}",
    "rpm_version": "${version}",
    "rpm_arch": "${arch}",
    "rpm_filename": "${rpm}"
EOF
done

CHANGECOUNT=$((POSTCOUNT-PRECOUNT))

cat << EOF | log
  "message_type": "result",
  "exit_code": ${RET},
  "pre_count": ${PRECOUNT},
  "post_count": ${POSTCOUNT},
  "change_count": ${CHANGECOUNT}
EOF

# Turn the new-line separated list of changes into a comma-separated list of quoted strings
LIST=`sed 's/^/"/;s/$/"/;s/^""$//' <(echo "${CHANGELIST}") | /usr/bin/tr '\n' ',' | sed 's/,$//'`

cat << EOF | log > $OUTPUT
  "message_type": "result",
  "exit_code": ${RET},
  "pre_count": ${PRECOUNT},
  "post_count": ${POSTCOUNT},
  "changes": [${LIST}]
EOF
